// -----------------------------------------------------------
// Project : Practica 2
// Autor : R. Mitel Ardelean
// Creation: Noviembrie 2018
// -----------------------------------------------------------
// File Name: main.c

#include <LPC214x.H> 
#include <string.h>
#include "lcd.h"
#include "mserial.h"

#define BUFFER_SIZE 500 //buffer size
#define MILLIS_CONSTANT 12000 //number of cycles for a millisecond: (clock is 12MHZ)
#define LCD_SIZE 16

// prototypes
void wait(int count); //will wait for some cycles, doing nothing
void addToBuffer(char c); // adds a character to the memory buffer
char * maxp(char *a, char *b); // max between 2 pointers
__irq void EXT_IRQ (void); // external interrupt function
__irq void T0isr(void); //timer interrupt function
void init_eint2(void); //external interrupt initialization 
void init_buzzer(void); // buzzer initialization
void buzz_time(int millis); // start the buzzer and schedule it to stop after millis seconds, using the timer
void bufferInit(); //clears the screen and initialize / resets the buffer

// !! Added 'serialInputAvailable' function in mserial library so that we can pool for new serial data without blocking

// global variables
char buffer[BUFFER_SIZE]; // declares the buffer in memory
char *bufferIt = buffer; // the buffer iterator points to the beggining of the part of the buffer that is currently showed on the lcd
char *bufferEnd = buffer; // points to the end of the buffer
int toReset = 0; // variable modified in the timer interrupt, used to signal when a reset is needed.

// setup function, initialize stuff. It is to be executed at the beggining of the program
void setup()
{
	serialInit();
	init_eint2();
	init_buzzer();
	bufferInit();
	init_lcd();
	lcd_clear();
	lcd_putstring(0, "Hello World"); // so that the first line is not empty
	serialSendLine("Conected"); // send a connection message trough serial, to check if everything is ok
} 


// loop function, to be executed in a loop while the program is running
void loop()
{
	while(serialInputAvailable()){ //while there are characters available to read from the serial buffer
		char c = serialReadChar(); // read the new character
		//the ';' cracter shows everything saved in the buffer (like the SW6 button) but does not reset it at the end
		if(c == ';')
		{
			bufferIt = buffer;
			return;
		}
		addToBuffer(c); //adds the new character to the buffer
		buzz_time(100); //activates a 100ms beep
	}
	if(bufferIt < bufferEnd - 16) // if the current displayed string is not the last substring of the buffer
	{
		bufferIt++; //shift to the left the showed substring, by 1 position
		wait(100000); //waits so that we can see the changes.
	}
	else if(toReset) // if all the string was displayed (we are at the end of the buffer) and we need a Reset
	{
		wait(200000); //we wait to mark the moment :D
		bufferInit(); //we reset the buffer
		toReset = 0; //we reset the flag
	}
	lcd_putstring(1, bufferIt); // refreshes the display
}

int main (void) 
{
	setup();
	while(1)
	{
		loop();
	}
}

//initializes the buffer, butting on the first [LCD_SIZE] pozitions the character ' ',
//so that we will begin writing from the right of the screen
void bufferInit()
{
	int i;
	bufferIt = buffer; // the iterator starts from the beggining of the buffer
	bufferEnd = buffer; // the we move the end of the buffer at the beggining too (clears the buffer).
	*bufferEnd = 0; //the last position marks the end of string, should be set to null
	//filling the first [LCD_SIZE] elements of the buffer with ' '
	for(i = 0; i <= LCD_SIZE; i++){
		addToBuffer(' ');
	}
	lcd_putstring(1, bufferIt); //we print to the LCD the reseted buffer (clears the line we are using)
}

//get maximum of 2 pointers (the last one)
char * maxp(char *a, char *b)
{
	if(a > b){
		return a;
	}
	return b;
}

//adds a character to the buffer
void addToBuffer(char c)
{
	//treating the buffer overflow by resetting the buffer
	//TODO: implement circular buffer
	if(bufferEnd - buffer >= BUFFER_SIZE-1){
		bufferInit();
	}
	*bufferEnd = c; //adds the new character
	bufferEnd++; //increases the buffer size
	*bufferEnd = 0; //puts NULL at the end of string
}

///just to nothing for a litle while -> this is ugly, TODO: implement the delay with timer
void wait(int count)
{
  int j=0,i=0;
  for(j=0;j<count;j++){
  	for(i=0;i<35;i++);
  }
}

///initialize the external interrupt on the button
void init_eint2(void){
	PINSEL0 		|= 2<<30;
	VICVectCntl0 	= 0x00000030;
	VICVectAddr0 	= (unsigned)EXT_IRQ; //when the interrupt fires, activates the interrupt function
	VICIntEnable 	|= 1<<16;
}

//initialize the buzzer and the timer needed to stop it
void init_buzzer(void){
	IODIR1 = 1<<25;
	IOSET1 = 1 << 25;
	T0PR = 0x00000001;
	T0TCR = 0x00000002;
	T0MCR = 3;
	
	VICVectAddr4 = (unsigned)T0isr; // the interrupt from the timer will stop the buzzer
	VICVectCntl4 = 0x00000024;
	VICIntEnable |= 0x00000010;
	
}

//starts the buzzer, and schedules the timer to stop it after [millis] milliseconds
void buzz_time(int millis){
	T0MR0 = millis*MILLIS_CONSTANT-1; // calculates the number of clock ticks coresponding to the clock speed
	IOCLR1 = 1 << 25; // starts buzzing
	T0TCR = 1; //activates the timer
}

// stops the buzzer, stops and resets the timer
void T0isr (void) __irq{
	IOSET1 = 1 << 25; 
	T0IR |= 0x00000001; 
	T0TCR = 2;
	VICVectAddr = 0x00000000;
}

// when the button was pressed and this function is activated
__irq void EXT_IRQ (void) {
	bufferIt = buffer; //start displaying the buffer from the start
	toReset = 1; //flags that after the buffer finishes displaying, it should be reseted
	EXTINT |= 1 << 2; //resets the interrupt flag
	VICVectAddr 	= 0x00000000; //returns to the normal flow of the program
}