//--------------------------------------------------------------
//--------------------------------------------------------------

#include <LPC21xx.H>
#include "mserial.h"

#define CR     '\n'

///checks if the serial input is available
int serialInputAvailable(void){
	return (U1LSR & 0x01);
}
//--------------------------------------------------------------
char serialReadChar(void){

  while (!(U1LSR & 0x01)); 	//espera por un caracter 
  return (U1RBR);  					//
}

//--------------------------------------------------------------
int serialSendChar	(char ch){                 

  while (!(U1LSR & 0x20)); 	// mientras el puerto no este vac�o
    U1THR = ch;
  return (0); 							// envia car�cter por puerto	
}

//--------------------------------------------------------------
int serialSendString (char *s) {
	char ch;									// mientras no encuentre a un "null"
	while ((ch=*s++)!='\0') {
		serialSendChar(ch);
	}
	return 0;
}

int serialSendLine(char *s){
	serialSendString(s);
	serialSendChar('\n');
	return 0;
}

//initialisar el terminal serie
void serialInit(void){

	PINSEL0 = 0x00050000;  // Enable RxD1 and TxD1   U1LCR         
	U1LCR = 0x83;          // 8 bits, no Parity, 1 Stop bit    
	U1DLL = 97;            // 9600 Baud Rate @ 15MHz PCLK 
	U1LCR = 0x03;          // DLAB = 0 
}





