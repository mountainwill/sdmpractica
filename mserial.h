


#ifndef _MSERIAL_H
#define _MSERIAL_H

// --------------------------------------------------
// Prototipos de funciones
// --------------------------------------------------

void serialInit(void);
char serialReadChar(void);
int  serialSendChar (char ch);
int  serialSendString (char *s); 

int serialInputAvailable(void);
int serialSendLine(char *s);




#endif  // _MSERIAL_H
