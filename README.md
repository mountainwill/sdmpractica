Repository: https://bitbucket.org/mountainwill/sdmpractica/

Program description: Sistemas Digitales y Microprocesadores practica 2
Program Authors: Razvan Mitel Ardelean & Daniel Calaver
Description & comentaries: Razvan Mitel Ardelean.

For this practice we started from the demo application: lcd_mensajes, so that's the titile of the project.
We used the example libraries "mserial.h" and "lcd.h".
We added the "serialInputAvailable" function in mserial.h so we could do non-blocking pooling of the serial input
We used a buffer where we saved in memory all the character that were submited.
We kept an iterator bufferIt, that saved from what position of the buffer was the string being displayed on the lcd.

The program has 3 pats:
1. Pooling from serial for new characters, and adding them to the buffer
2. Using a timer, we scheduled the buzzer to beep a certain number of milliseconds without interrupting the program
3. Using an external interrupt on the SW6 button, at any point in the execution of the program, when the button was pressed the bufferIt was put at the begging of the buffer, so that all the text in the buffer will be displayed rolling from right to left. A reset flag is also used so that after it finishes displaying the whole buffer (bufferIt reached the end-LCD_SIZE) we reset the buffer, and do going to the initial state.
